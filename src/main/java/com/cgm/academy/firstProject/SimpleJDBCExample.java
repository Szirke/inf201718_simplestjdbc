package com.cgm.academy.firstProject;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;

@SpringBootApplication
public class SimpleJDBCExample implements CommandLineRunner {

    private final static String H2_DB_URL = "jdbc:h2:mem:testdb";
    private final static String H2_DB_USER = "sa";
    private final static String H2_DB_PASS = "";
    private final static String H2_DB_DRIVER = "org.h2.Driver";

    public static void main(String[] args) {
        SpringApplication.run(SimpleJDBCExample.class, args);
    }

    @Override
    public void run(String... args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            // zarejestrowanie sterownika
            Class.forName(H2_DB_DRIVER).newInstance();
            // otwarcie połączenia z bazą
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(H2_DB_URL, H2_DB_USER, H2_DB_PASS);
            // Wykonanie zapytania
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql = "SELECT id, firstName, lastName FROM Person";
            ResultSet rs = stmt.executeQuery(sql);

            // Obrabianie wyników
            while (rs.next()) {
                //pobieranie danych z resultSet na podstawie nazw kolumn
                int id = rs.getInt("id");
                String first = rs.getString("firstName");
                String last = rs.getString("lastName");
                //Wypisanie odczytanych danych
                System.out.print("ID: " + id);
                System.out.print(", FirstName: " + first);
                System.out.println(", LastName: " + last);
            }
            // zamknięcie połączenia
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //problem z JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException e) {
            // wyjątek przy Class.forName
            e.printStackTrace();
        } catch (InstantiationException | IllegalAccessException e) {
            // wyjątek przy .newInstance()
            e.printStackTrace();
        } finally {
            // bez względu na powodzenie/porażkę, zamykamy połączenie
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
                // błąd przetwarzania polecenia
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                // błąd przetwarzania polecenia
                se.printStackTrace();
            }
        }
    }

}
